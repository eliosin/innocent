![](https://elioway.gitlab.io/eliosin/innocent/elio-innocent-logo.png)

> duck sit rank runt rollicks **Nicola Milton**

# innocent ![alpha](https://elioway.gitlab.io/eliosin/icon/devops/alpha/favicon.ico "alpha")

**innocent** is a minimal and acceptable classless css framework with opinions about how tags like table and form should look on **eliosin**.

**god**'s first commandment is "style your own forms", because god is only interested in layout.

**eve** is a boilerplate to create a style from scratch.

**bushido** and **sins** (will) be fully developed style packs for **eliosin**.

**innocent** leaves you free to ignore the first commandment: "style your own forms"; balancing minimal attention with acceptable formatting.

@TODO This all lacks clarity. **innocence** should be a plug in, like **eve** and **adon** are plug ins. **god** should be a plug in. plug ins should respect **eliosin**'s settings, adding settings only as a last resort. Many of **god**'s settings should be moved to **sin** settings. This concept of a plug in to **god** should be clearer from the start.

@TODO Off the bat, create a **sin** app - and make this the where **gods** settings come from. eliosin's settings

- [innocent Documentation](https://elioway.gitlab.io/eliosin/innocent)
- [innocent Demo](https://elioway.gitlab.io/eliosin/innocent/demo.html)

## Installing

```
yarn add  @elioway/innocent --dev
npm install @elioway/innocent --save-dev
```

- [Installing innocent](https://elioway.gitlab.io/eliosin/innocent/installing.html)

## Seeing is Believing

```shell
git clone https://gitlab.com/eliosin/innocent.git
cd innocent
npm i|yarn
gulp
```

## Nutshell

### `gulp`

### `npm run test`

### `npm run prettier`

- [innocent Quickstart](https://elioway.gitlab.io/eliosin/innocent/quickstart.html)
- [innocent Credits](https://elioway.gitlab.io/eliosin/innocent/credits.html)

![](https://elioway.gitlab.io/eliosin/innocent/apple-touch-icon.png)

## License

[HTML5 Boilerplate](LICENSE.txt) [Tim Bushell](mailto:theElioWay@gmail.com)
