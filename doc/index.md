<aside>
  <dl>
  <dd>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
  up they rose</dd>
  <dd>Soon found their eyes how opened, and their minds</dd>
  <dd>How darkened; innocence, that as a veil</dd>
  <dd>Had shadowed them from knowing ill, was gone;</dd>
  <dd>Just confidence, and native righteousness,</dd>
  <dd>And honour, from about them, naked left</dd>
  <dd>To guilty Shame</dd>
</dl>
</aside>

**god**'s first commandment is "style your own forms", because **god** is only interested in layout.

**eve** is not a fully blown style pack, more a platform from which you could create one from scratch.

**bushido** and **sins** contain fully developed style packs for eliosin.

eliosin isn't the only classless css framework, but it is the one focused on layout. **innocent** fills the space between "style your own forms"; balancing minimal with acceptable.

Add **innocent** to your eliosin projects and don't worry about the rest.

**innocent** is classless css with opinions about how tags like table and form should be laid out eliosin's three _prong_ approach.

**innocent** meets the same goals as other classless frameworks like **Marx** and **Tacit**, and so I'll just copy what Matthew Blode said about Marx:

- Built atop of Normalize.css, meaning all browsers render consistently.
- Customise using a SCSS preprocessor and **the elioWay**.
- Responsive and mobile-friendly.
- Clean, beautiful typography.
- Forms, tables, buttons and navigation.
- Zero classes, so it already works with your HTML.
- Drop-dead not totally terrible.
- It just works with eliosin.

<div>
  <a href="demo.html" target="_demo">
  <button>Demo</button>
</a>
</div>

# Nutshell

- tagNames are selectors, not classNames.
- **innocent** is an eliosin theme.
- **innocent** is a css stylesheet to format common HTML tags and the way they sit in **god**'s layout.

# See also

- [eliosin/bushido](/eliosin/bushido)
- [eliosin/sins](/eliosin/sins)
