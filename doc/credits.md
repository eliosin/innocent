# innocent Credits

## Core! Ta very much.

- [marx:mblode](https://github.com/mblode/marx)

  - Private property is a crime and therefore I didn't steal mblode's demo HTML.

- [codepen:Trezy](https://codepen.io/trezy/pen/mvaLt)

## These were useful

I'd have preferred to use an existing, classless framework; I even tried incorporating many of ones listed here:

- [awesome-css-frameworks:troxler](https://project-awesome.org/troxler/awesome-css-frameworks)
- [classless-css:dbohdan](https://github.com/dbohdan/classless-css)
- <https://codepen.io/zakkain/pen/sFhCq>

None of them quite worked for eliosin - not the way I wanted them to - but they might work for you and the following were my best candidates.

- [awsm:igoradamenko](https://github.com/igoradamenko/awsm.css)
- [marx:mblode](https://github.com/mblode/marx)
- [tacit:yegor256](https://github.com/yegor256/tacit.git)

## Artwork

- [wikimedia:Angel_playing_a_harp](https://commons.wikimedia.org/wiki/File:Angel_playing_a_harp.jpg)
- <https://publicdomainvectors.org/en/free-clipart/Fetus-vector-image/70640.html>
