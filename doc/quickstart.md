# Quickstart innocent

- [innocent Prerequisites](/eliosin/innocent/prerequisites.html)
- [Installing innocent](/eliosin/innocent/installing.html)

## Nutshell

- [Install innocent](/eliosin/innocent/installing.html)

## How to write your HTML

**innocent** is a partially opinionated css framework. The eliosin framework comes with a number of laws which **innocent** acknowledges and obeys.

**innocent** only styles tagNames which appear directly inside a single container. It styles them in preparation for whether that tagName appears in _heaven_, _hell_ and _pillar_. It expects a minimal tagName approach.

This document shows you how to write HTML which makes the most of "innocent" styles.

The innocent factor is a description of what choices **innocent** has taken.

The golden rule is no unnecessary nesting. Don't use `div` tags to nest tags which aren't already nested.

## Inline tags

**innocent** relies on **Normalize.css** for most of it's layout. The **eliosin** framework assumes the `p` tag will always be used directly inside the _container_, and most inline tags across **innocent** are left alone unless specified below.

### innocent factor

- `kbd` tag is styled like a pill with a background and rounded corner. `kbd` would make a good tag.

## Lists

`ul`, `li` and `dl` should be written as normal.

### innocent factor

- `dl` is padded the `$personal-space` setting.
- `dt` is bold using the `$god-spoke` font-size setting.
- `dt` is padded above using the `$personal-space` setting.
- `dd` is padded below the using `$personal-space` setting.

## Code

`pre` and `code` should be written as normal.

### innocent factor

- `pre` uses the `$god-blushed` setting as a background.

## Figure

**innocent** assumes there will be an `img` inside a figure.

```html
<figure>
  <img src="image1.png" />
  <figurecaption>Image 1: Paradise Lost</figurecaption>
  <p>Anything else you want</p>
</figure>
```

### innocent factor

- `figure` gets a border matching the `$god-border` setting.
- `figure` gets a padding using the `$god-space` setting.
- `figurecaption` uses the `$god-head` font-family setting.
- `img` is `100%` width, filling the `figure` frame.

## Images

Remember, this only applies when you put an `img` tag directly inside the container.

```html
<img src="image1.png" />
```

### innocent factor

- `img` is `100%` wide, height `auto`, filling the width of its prong in _heaven_, _hell_ or _pillar_

## Menu and Nav

**innocent** suggests you write HTML **the elioWay**, and keep tags to the bare minimum.

For a simple menu, keep it as simple as possible. **god** hates HTML fluff.

```html
<menu>
  <a href="link1">link1</a>
  <a href="link2">link2</a>
  <a href="link1">link1</a>
  <a href="link1">link1</a>
</menu>

<nav>
  <a href="link1">link1</a>
  <a href="link2">link2</a>
  <a href="link1">link1</a>
  <a href="link1">link1</a>
</nav>
```

### innocent factor

- `a` tags which are direct children of the root `menu` are aligned horizontally across the page.
- `a` tags which are direct children of the root `nav` are aligned vertically down the page.

You'll notice, or your should, that I said "direct children of the root" `menu` or `nav`. This is because the root `menu` and `nav` are treated specially. They are already handled by **god**.

Once the parent `menu` or `nav` tag has been set, `menu` and `nav` can also be used in combination to create sub menus.

```html
<nav|menu>
  <a href="link1">link1</a>
  <a href="link2">link2</a>
  <menu>
    <label>submenu1</label>
    <nav>
      <a href="submenu1link1">submenu1 link1</a>
      <menu>
        <label>submenu1 submenu2</label>
        <nav>
          <a href="submenu2link1">submenu2 link1</a>
          <menu>
            <label>submenu2 submenu3</label>
            <nav>
              <a href="submenu3link1">submenu3 link1</a>
              <menu>
                <label>submenu3 submenu4</label>
                <nav>
                  <a href="submenu4link1">submenu4 link1</a>
                  <a href="submenu4link2">submenu4 link2</a>
                </nav>
              </menu>
            </nav>
          </menu>
        </nav>
      </menu>
    </nav>
  </menu>
</nav|menu>
```

### innocent factor

- Nested `menu` tags should be siblings of `a` tags.
- A nested `menu` should have a `label` and a `nav` tag.
- The `label` is displayed and the `nav` tag is hidden.
- Onhover nested `menu` tags will reveal the `nav`.
- You can keep on nesting `menu` inside `nav` indefinately

## Tables

`table` should be written as normal.

```html
<table>
  <tr>
    <td>cell1</td>
    <td>cell2</td>
  </tr>
  <tr>
    <td>cell3</td>
    <td>cell4</td>
  </tr>
</table>
```

### innocent factor

- `table` is the exact width of its _prong_.
- `th` and `td` get the `$inline-padding` setting for vertical padding.
- `th` and `td` get the `$personal-space` setting for horizontal padding.
- Gets toothpasted `tr` rows using the `$toothpaste-color` setting (if not 'none').

`thead`, `tbody` and `tfoot` give **innocent** permission to get opinionated.

```html
<table>
  <thead>
    <tr>
      <th></th>
      <th>head1</th>
      <th>head2</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>rowhead1</th>
      <td>cell1</td>
      <td>cell2</td>
    </tr>
    <tr>
      <th>rowhead2</th>
      <td>cell3</td>
      <td>cell4</td>
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <th></th>
      <th>footer1</th>
      <th>footer2</th>
    </tr>
  </tfoot>
</table>
```

### innocent factor

- `th` in `thead`, `tbody` and `tfoot` uses the `$god-head` font-family setting.
- `th` in `thead` has a bottom border twice the `$god-border` setting.
- `th` in `tfoot` has a bottom border twice the `$god-border` setting.
- `td` and `th` in `tbody` have a border using the `$god-border` setting.
- `th` in `tfoot` text is aligned `right`.

## Forms

**innocent** is more opinionated about forms than anything else. `labels` and `ipputs` can appear directly inside the `form` field.

```html
<form>
  <legend>Fill me in!</legend>
  <label for="name">Text Input:</label>
  <input type="text" placeholder="Text Input" />
  <label for="name">Date Input:</label>
  <input type="date" />
  <label for="textarea">Textarea:</label>
  <textarea rows="4" cols="27" placeholder="Text Area"></textarea>
</form>
```

### innocent factor outside of a fieldset

- `label` gets the `$personal-space` setting as margin top.
- `legend` gets the `$god-space` setting for vertical padding.
- `legend` gets the `$god-spoke` font-size setting.
- `label` and `legend` use the `$god-head` font-family setting.
- `input` gets the `$inline-padding` setting for vertical padding.
- `input` gets the `$personal-space` setting for horizontal padding.
- Outside of a `fieldset` `label` and `input` are inline.

You can also use a `fieldset` to give **innocent** permission to get opinionated.

```html
<form>
  <fieldset>
    <legend>fieldset1</legend>
    <label for="name">Text Input:</label>
    <input type="text" placeholder="Text Input" />
    <label for="name">Date Input:</label>
    <input type="date" />
  </fieldset>
  <fieldset>
    <legend>fieldset2</legend>
    <textarea rows="4" placeholder="Text Area"></textarea>
  </fieldset>
</form>
```

### innocent factor in a fieldset

- `fieldset` gets the `$personal-space` setting as margin top.
- `fieldset` gets the `$god-border` setting.
- `fieldset` gets the `$personal-space` setting for horizontal padding.
- `fieldset` has no vertical padding, let the margins of its children handle space.
- `label` displays `block`; on a new line.
- `input` gets 100% width
- `input` box-sizing using `border-box`.

Use either type of button.

```html
<form>
  <input type="submit" value="Submit" />
  <button>Submit</button>
</form>
```

### button innocent factor

- `input[type=submit|reset]` look like normal buttons
- `button` uses the `$god-head` and `$god-border` settings.
- `button` uses the `$god-head` and `$god-border` settings.
- `button` gets the `$personal-space` setting for vertical padding.
- `button` gets the `$god-space` setting for horizontal padding.
- Onhover `button` gets the `$pillar-color` setting as a background for `white` text.

## Header

It is the opinion of **innocent** that a header should be used like a jumbotron.

### innocent factor

- `header` gets the `$god-space` setting as margin.
- `header` gets the `$personal-space` setting for padding.
- Contents `centre` aligned.
- `h1` to `h6` get the `$personal-space` setting for vertical margin.

## What we learnt

1. What to expect from **innocent**.

2. How to write HTML which makes the most of **innocent**.

If you import **innocent**'s `settings.scss` file, you can rely on its default. If you want to override these settings, add them to the bottom of your eliosin project's `settings.scss` file.

## Next

Start using **innocent** in your **eliosin** projects!
