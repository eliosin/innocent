# Installing innocent

- [innocent Prerequisites](/eliosin/innocent/prerequisites.html)
- [Installing god](/eliosin/god/installing.html)

## Coming soon: Installing with generator-sin

Installing with eliosin's own **generator-sin** is the recommended way.

- [Installing generator-sin](/eliosin/generator-sin/installing.html)

```
yo sin:innocent
gulp
```

## Install with NPM or Yarn as a dependency of your current project

Add **innocent** and **god** as NPM packages.

```shell
npm install @elioway/god @elioway/innocent --save-dev
yarn add @elioway/god @elioway/innocent --dev
```

## Add it to your compile funnel

- Locate the primary `X.scss` build file within your current project stylesheets.

  - For instance, the one targeted by your compile tools in the `gruntfile`, `gulpfile` or other.

- Add the following to your current project SASS build file:

  - Notice we are using **innocent**'s extra settings. (See [Quickstart](/eliosin/innocentquickstart.html))

```scss
@import "../node_modules/@elioway/innocent/stylesheets/settings";
@import "../node_modules/@elioway/god/stylesheets/theme";
@import "../node_modules/@elioway/innocent/stylesheets/theme";
```

### Using your own settings

- Copy the `settings.scss` file from `./node_modules/@elioway/innocent/stylesheets/settings.scss`

  - This will insure you get **innocent**'s extra settings. (See [Quickstart](quickstart.html))

- Paste it alongside `X.scss` into, for instance, `my_innocent_settings.scss`

- Change the SASS build file making sure your settings gets called before **god**'s theme:

```scss
@import "my_innocent_settings";
@import "../node_modules/@elioway/god/stylesheets/theme";
@import "../node_modules/@elioway/innocent/stylesheets/theme";
```

## Nah! I'll just use the dist

```html
<link
  rel="stylesheet"
  href="node_modules/@elioway/innocent/dist/css/innocent.min.css"
/>
```
