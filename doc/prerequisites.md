# innocent Prerequisites

- **innocent** understood [No!](/eliosin/innocent)
- [Check the Demo](/eliosin/innocent/demo.html)
- **eliosin** prerequisites met? [No!](/eliosin/installing.html)
- It might be useful if you have completed **god** Quickstart [Oh](/eliosin/god/quickstart.html)

- [god Prerequisites](/eliosin/god/prerequisites.html)

- [Installing god](/eliosin/god/installing.html)
